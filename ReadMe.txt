To run open Assets/SolarSytenScene.unity

This project models the solar system in unity game engine.
Planets orbit around the sun at a rate of 70 days/second. Orbit paths are based on real postions of planets taken from nasa calculations https://ssd.jpl.nasa.gov/horizons.cgi#top . Each planet has several nodes in it's path which it travels to before targeting the next node.
Distances between bodies have a 1:1 ration with real life using a scale of 1 Unity Unit = 0.1 Astronmical Unit(The averager distance between the sun and the earth)

Sizes of objects are not to scale
For the sun 1 Astronmical Unit = 1,000 Unity Units
For inner planets(Mercury,Venus,Earth,Mars) 1 Astronmical Unit = 20,000 Unity Units
For outer planets(Jupiter,Saturn) 1 Astronmical Unit = 10,000 Unity Units
A consequence of this scaling is that the outer planets look larger than the Sun.

Movements can be a bit janky for the inner planets. This could be improved by introducing additional nodes or decreasing the speed (days/second)

Future improvements could include introducing colors/textures for the planets and automatically generating nodes based on a formula for eliptical orbits