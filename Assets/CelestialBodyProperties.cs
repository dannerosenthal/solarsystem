﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "CelestialBodyProperties")]
public class CelestialBodyProperties : ScriptableObject {
    public string bodyName;
    public float radius;
    public Color color;
    public CelestialPostion[] positions;
}
[Serializable]
public class CelestialPostion
{
    public int day;
    public Vector3 position;
}
