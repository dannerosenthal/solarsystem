﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelestialBodyScript : MonoBehaviour {

    public CelestialBodyProperties properties;
    public float daysPerSecond;
    public float day;
    [SerializeField]
    MeshFilter filter;
    //[SerializeField]
    //Rigidbody body;
    private int positionIndex = 0;
    void Start()
    {
        Initialize();
    }
    void OnValidate()
    {
        Initialize();
    }
    // Use this for initialization
    void Initialize()
    {
        //Mesh mesh = SphereGenerator.getSphereMesh();
        if (properties != null) {
            if (filter == null)
            {
                GameObject meshObj = new GameObject("mesh");
                meshObj.transform.SetParent(gameObject.transform);
                meshObj.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshObj.AddComponent<MeshFilter>();
                filter = meshObj.gameObject.GetComponent<MeshFilter>();
                filter.transform.position = new Vector3(0, 0, 0);
            }
            if(filter.sharedMesh != null) filter.sharedMesh.Clear();
            filter.sharedMesh = SphereGenerator.getSphereMesh(properties.radius, 15, 15);

            if (properties.positions.Length > 0)
            {
                gameObject.transform.position = properties.positions[0].position;
            }
            if (gameObject.GetComponent<Rigidbody>() == null) gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().useGravity = false;
                
        }
        daysPerSecond = 70;
	}

    void UpdateVelocity()
    {
        day += daysPerSecond * Time.deltaTime;
        incrimentPostion();
        float deltaDays = nextPosition().day - dayInYear();
        Vector3 velocity = (nextPosition().position - transform.position) / (deltaDays / daysPerSecond);
        Debug.Log(velocity.ToString("F2"));
        gameObject.GetComponent<Rigidbody>().velocity = velocity;
        Debug.Log(gameObject.GetComponent<Rigidbody>().velocity.ToString("F2"));
        Debug.Log(day);
        Debug.Log(positionIndex);


    }
    void incrimentPostion()
    {
        int maxItterations = 10;
        int counter = 0;
        while (dayInYear() >= nextPosition().day || (positionIndex == properties.positions.Length - 2 && dayInYear() < priorPosition().day))
        {
            if (properties.positions.Length > positionIndex + 1)
                positionIndex++;
            else
                positionIndex = 0;
            counter++;
            if (counter > maxItterations) break;
        }
    }

    private CelestialPostion currentPostion()
    {
        return properties.positions[positionIndex];
    }
    private CelestialPostion priorPosition()
    {
        if (positionIndex == 0)
            return properties.positions[properties.positions.Length - 1];
        else
            return properties.positions[positionIndex - 1];
    }
    private CelestialPostion nextPosition()
    {
        if (positionIndex == properties.positions.Length - 1)
            return properties.positions[0];
        else
            return properties.positions[positionIndex + 1];
    }
    public int LengthOfYear()
    {
        return properties.positions[properties.positions.Length - 1].day;
    }
    public float dayInYear()
    {
        return day % LengthOfYear();
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateVelocity();
	}
}
